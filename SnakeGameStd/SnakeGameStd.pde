int gridSize = 30;
Snake snake;
Food food;
void setup() {
  size(600, 600);
  snake = new Snake();
  food = new Food(snake);
}

void draw() {
  background(51);
  food.show();
  snake.show();
  if (food.pos.equals(snake.pos)) {
    snake.grow();
    food.repos();
  }
  if (frameCount % 5 == 0)
    snake.move();
}

void keyPressed() {
  if (key == 'w')
    snake.updateDir(0, -1);//negative y goes upwards
  else if (key == 's')
    snake.updateDir(0, 1);//positive y goes downwards
  else if (key == 'd')
    snake.updateDir(1, 0);
  else if (key == 'a')
    snake.updateDir(-1, 0);
}
