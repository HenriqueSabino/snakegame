class Snake {
  private int size = 4;
  private PVector dir;
  public ArrayList<Part> parts;
  public PVector pos;
  public int score = 0;

  Snake() {
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
  }

  void show() {
    fill(255);
    noStroke();
    for (Part p : parts) {
      rect(p.x, p.y, gridSize - 1, gridSize - 1);
    }
  }

  void move() {
    //makes sure it's not moving diagonally
    if (abs(dir.x) == 1 && abs(dir.y) == 1) {
      if (random(1) >= 0.5)
        dir.x *= 0;
      else
        dir.y *= 0;
    }
    //updates all body parts positions
    for (int i = size - 1; i >= 1; i--) {
      parts.get(i).x = parts.get(i - 1).x;
      parts.get(i).y = parts.get(i - 1).y;
    }
    //updates the head position
    parts.get(0).x += dir.x * gridSize;
    parts.get(0).y += dir.y * gridSize;

    checkEdges();
    pos = new PVector(parts.get(0).x, parts.get(0).y);
    checkDeath();
  }

  void updateDir(int x, int y) {
    //prevents the player to go backwards
    //if the current position plus the "next step" equals the position of the snake's "neck"
    dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
    dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
  }

  void checkEdges() {
    if (parts.get(0).x >= width)
      parts.get(0).x = 0;
      //reset();
    else if (parts.get(0).x < 0)
      parts.get(0).x = width;
      //reset();
    if (parts.get(0).y >= height)
      parts.get(0).y = 0;
      //reset();
    else if (parts.get(0).y < 0)
      parts.get(0).y = height;
      //reset();
  }

  void grow() {
    size++;
    score += 10;
    println();
    parts.add(new Part((int)pos.x, (int)pos.y));
  }

  void reset() {
    score = 0;
    size = 4;
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
  }

  void checkDeath() {
    for (int i = 1; i < size; i++) {
      if (pos.x == parts.get(i).x && pos.y == parts.get(i).y) {
        reset();
      }
    }
  }
}
