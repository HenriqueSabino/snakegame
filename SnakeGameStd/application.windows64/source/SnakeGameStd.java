import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SnakeGameStd extends PApplet {

int gridSize = 30;
Snake snake;
Food food;
public void setup() {
  
  snake = new Snake();
  food = new Food(snake);
}

public void draw() {
  background(51);
  food.show();
  snake.show();
  if (food.pos.equals(snake.pos)) {
    snake.grow();
    food.repos();
  }
  if (frameCount % 5 == 0)
    snake.move();
}

public void keyPressed() {
  if (key == 'w')
    snake.updateDir(0, -1);//negative y goes upwards
  else if (key == 's')
    snake.updateDir(0, 1);//positive y goes downwards
  else if (key == 'd')
    snake.updateDir(1, 0);
  else if (key == 'a')
    snake.updateDir(-1, 0);
}
class Food {
  PVector pos;
  Snake snake;
  Food(Snake snake) {
    pos = new PVector();
    this.snake = snake;
    repos();
  }

  public void repos() {
    pos.x = floor(random(width / gridSize)) * gridSize;
    pos.y = floor(random(height / gridSize)) * gridSize;

    for (Part p : snake.parts) {
      if (pos.x == p.x && pos.y == p.y)
        repos();
    }
  }

  public void show() {
    fill(255, 0, 0);
    ellipseMode(CORNER);
    ellipse(pos.x, pos.y, gridSize - 1, gridSize - 1);
  }
}
class Part {
  int x, y;
  Part(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
class Snake {
  private int size = 4;
  private PVector dir;
  public ArrayList<Part> parts;
  public PVector pos;
  public int score = 0;

  Snake() {
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
  }

  public void show() {
    fill(255);
    noStroke();
    for (Part p : parts) {
      rect(p.x, p.y, gridSize - 1, gridSize - 1);
    }
  }

  public void move() {
    //makes sure it's not moving diagonally
    if (abs(dir.x) == 1 && abs(dir.y) == 1) {
      if (random(1) >= 0.5f)
        dir.x *= 0;
      else
        dir.y *= 0;
    }
    //updates all body parts positions
    for (int i = size - 1; i >= 1; i--) {
      parts.get(i).x = parts.get(i - 1).x;
      parts.get(i).y = parts.get(i - 1).y;
    }
    //updates the head position
    parts.get(0).x += dir.x * gridSize;
    parts.get(0).y += dir.y * gridSize;

    checkEdges();
    pos = new PVector(parts.get(0).x, parts.get(0).y);
    checkDeath();
  }

  public void updateDir(int x, int y) {
    //prevents the player to go backwards
    //if the current position plus the "next step" equals the position of the snake's "neck"
    dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
    dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
  }

  public void checkEdges() {
    if (parts.get(0).x >= width)
      parts.get(0).x = 0;
      //reset();
    else if (parts.get(0).x < 0)
      parts.get(0).x = width;
      //reset();
    if (parts.get(0).y >= height)
      parts.get(0).y = 0;
      //reset();
    else if (parts.get(0).y < 0)
      parts.get(0).y = height;
      //reset();
  }

  public void grow() {
    size++;
    score += 10;
    println();
    parts.add(new Part((int)pos.x, (int)pos.y));
  }

  public void reset() {
    score = 0;
    size = 4;
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
  }

  public void checkDeath() {
    for (int i = 1; i < size; i++) {
      if (pos.x == parts.get(i).x && pos.y == parts.get(i).y) {
        reset();
      }
    }
  }
}
  public void settings() {  size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "SnakeGameStd" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
