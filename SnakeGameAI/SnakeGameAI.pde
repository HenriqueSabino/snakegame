import java.util.*;

int gridSize = 30;
Snake snake;
ArrayList<Snake> population;
Food food;
boolean createNew = false;
int current = 0;
int popSize = 100;
int generation = 1;
float maxScore = 0;

void setup() {
  size(600, 600);
  population = new ArrayList<Snake>();
  for (int i = 0; i < popSize; i++) {
    population.add(new Snake());
  }
  snake = population.get(current);
  food = new Food(snake);
}

void draw() {
  if (snake.score > maxScore)
    maxScore = snake.score;
  //if the snake passes too much time without eating
  //then kill it and divide its fitness by some amout
  if (frameCount - snake.lastAte >= 100 * snake.size) {
    snake.reset(true);
  }
  if (createNew) {
    current++;
    if (current == popSize) {
      repopulate();
      current = 0;
    }
    snake = population.get(current);
    snake.lastAte = frameCount;
    createNew = false;
  }
  background(51);
  fill(255);
  textSize(20);
  textAlign(LEFT, CENTER);
  text("generation - " + generation, 15, 25);
  text("population - " + popSize, 15, 50);
  text("Highscore - " + maxScore, 15, 75);
  food.show();
  snake.show();
  if (food.pos.equals(snake.pos)) {
    snake.grow();
    food.repos();
  }
  //if (frameCount % 5 == 0)
  snake.move();
}

void repopulate() {
  float sum = 0;
  ArrayList<Snake> nextPop = new ArrayList<Snake>();
  for (int i = 0; i < popSize; i++) {
    sum += population.get(i).fitness;
  }

  for (int i = 0; i < popSize; i++) {
    population.get(i).fitness /= sum;
  }
  
  population.sort(new SnakeComparator());
  
  for (int i = 0; i < popSize; i++) {
    nextPop.add(new Snake(population));
  }

  population = nextPop;
  generation++;
}
