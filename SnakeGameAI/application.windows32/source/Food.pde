class Food {
  PVector pos;
  Snake snake;
  Food(Snake snake) {
    pos = new PVector();
    this.snake = snake;
    repos();
  }

  void repos() {
    pos.x = floor(random(width / gridSize)) * gridSize;
    pos.y = floor(random(height / gridSize)) * gridSize;

    for (Part p : snake.parts) {
      if (pos.x == p.x && pos.y == p.y)
        repos();
    }
  }

  void show() {
    fill(255, 0, 0);
    ellipseMode(CORNER);
    ellipse(pos.x, pos.y, gridSize - 1, gridSize - 1);
  }
}
