class Snake implements Comparable<Snake>{
  private int size = 4;
  private PVector dir;
  public ArrayList<Part> parts;
  public PVector pos;
  public int score = 0;
  public NeuralNetwork neuNet;
  public float fitness = 0;
  float minDist = 100000;
  //frame that it eats the food
  //avoids the snake to spins in circles
  float lastAte;

  Snake() {
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
    neuNet = new NeuralNetwork(6, 1, new int[] {10}, 4);
  }

  //genetic algorithm
  Snake(ArrayList<Snake> prevPop) {
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
    neuNet = new NeuralNetwork(6, 1, new int[] {10}, 4);

    Snake parent1 = prevPop.get(pickParent(prevPop, random(1)));
    Snake parent2 = prevPop.get(pickParent(prevPop, random(1)));

    //note that it does not need to inherit the weights of the input layer because there are no weights in the layer
    //inheriting the hidden layers weights and biases
    for (int i = 0; i < parent1.neuNet.hiddenLayers.size(); i++) {
      for (int j = 0; j < parent1.neuNet.hiddenLayers.get(i).size(); j++) {
        for (int k = 0; k < parent1.neuNet.hiddenLayers.get(i).get(j).inWeights.length; k++) {
          //mutation rate
          if (random(1) >= 0.03) {
            if (random(1) >= 0.5) {
              this.neuNet.hiddenLayers.get(i).get(j).inWeights[k] = parent1.neuNet.hiddenLayers.get(i).get(j).inWeights[k];
              this.neuNet.hiddenLayers.get(i).get(j).bias = parent1.neuNet.hiddenLayers.get(i).get(j).bias;
            } else {
              this.neuNet.hiddenLayers.get(i).get(j).inWeights[k] = parent2.neuNet.hiddenLayers.get(i).get(j).inWeights[k];
              this.neuNet.hiddenLayers.get(i).get(j).bias = parent2.neuNet.hiddenLayers.get(i).get(j).bias;
            }
          }
          //if it will mutate
          else {
            this.neuNet.hiddenLayers.get(i).get(j).inWeights[k] = random(-1, 1);
            this.neuNet.hiddenLayers.get(i).get(j).bias = random(-1, 1);
          }
        }
      }
    }

    //inheriting the outputs weights and biases
    for (int i = 0; i < parent1.neuNet.outputs.size(); i++) {
      for (int j = 0; j < parent1.neuNet.outputs.get(0).inWeights.length; j++) {
        //mutation rate
        if (random(1) >= 0.03) {
          if (random(1) >= 0.5) {
            this.neuNet.outputs.get(i).inWeights[j] = parent1.neuNet.outputs.get(i).inWeights[j];
            this.neuNet.outputs.get(i).bias = parent1.neuNet.outputs.get(i).bias;
          } else {
            this.neuNet.outputs.get(i).inWeights[j] = parent2.neuNet.outputs.get(i).inWeights[j];
            this.neuNet.outputs.get(i).bias = parent2.neuNet.outputs.get(i).bias;
          }
        }
        //if it will mutate
        else {
          this.neuNet.outputs.get(i).inWeights[j] = random(-1, 1);
          this.neuNet.outputs.get(i).bias = random(-1, 1);
        }
      }
    }
  }
  //genetic algorithm

  void show() {
    fill(255);
    noStroke();
    for (Part p : parts) {
      rect(p.x, p.y, gridSize - 1, gridSize - 1);
    }
  }

  void move() {  
    float foodDirX = food.pos.x - pos.x;
    float foodDirY = food.pos.y - pos.y;
    //distance to wall and distance to the food
    float[] inputs = new float[] {foodDirX, foodDirY, checkNextStep(1, 0), checkNextStep(-1, 0), checkNextStep(0, 1), checkNextStep(0, -1)};
    //sets the input to the Neural Network
    neuNet.setInputs(inputs);
    //gets the output of the Neural Network
    float[] nextDir = neuNet.getOutput();
    updateDir(nextDir[0], nextDir[1], nextDir[2], nextDir[3]);

    //updates all body parts positions
    for (int i = size - 1; i >= 1; i--) {
      parts.get(i).x = parts.get(i - 1).x;
      parts.get(i).y = parts.get(i - 1).y;
    }
    //updates the head position
    parts.get(0).x += dir.x * gridSize;
    parts.get(0).y += dir.y * gridSize;

    checkEdges();
    float currentDist = dist(pos.x, pos.y, food.pos.x, food.pos.y);
    if (currentDist < minDist)
      minDist = currentDist;

    pos = new PVector(parts.get(0).x, parts.get(0).y);

    checkDeath();
  }

  void updateDir(float u, float d, float l, float r) {
    FloatList temp = new FloatList();
    temp.append(u);
    temp.append(d); 
    temp.append(l); 
    temp.append(r);
    temp.sortReverse();
    if (temp.get(0) == u) {
      int x = 0, y = -1;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    } else if (temp.get(0) == d) {
      int x = 0, y = 1;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    } else if (temp.get(0) == l) {
      int x = -1, y = 0;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    } else if (temp.get(0) == r) {
      int x = 1, y = 0;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    }
  }

  void checkEdges() {
    if (parts.get(0).x >= width)
      //parts.get(0).x = 0;
      reset(false);
    else if (parts.get(0).x < 0)
      //parts.get(0).x = width;
      reset(false);
    if (parts.get(0).y >= height)
      //parts.get(0).y = 0;
      reset(false);
    else if (parts.get(0).y < 0)
      //parts.get(0).y = height;
      reset(false);
  }

  void grow() {
    size++;
    score += 10;
    lastAte = frameCount;
    parts.add(new Part((int)pos.x, (int)pos.y));
  }

  void reset(boolean spinning) {
    calcFitness();
    if (spinning)
      this.fitness /= 5000;
    createNew = true;
  }

  void checkDeath() {
    for (int i = 1; i < size; i++) {
      if (pos.x == parts.get(i).x && pos.y == parts.get(i).y) {
        reset(false);
      }
    }
  }

  int checkNextStep(int x, int y) {
    for (int i = 1; i < size; i++) {
      if (pos.x + x == parts.get(i).x && pos.y + y == parts.get(i).y) {
        return 0;
      }
      if (pos.x + x >= width)
        return 0;
      else if (pos.x + x < 0)
        return 0;
      if (pos.y + y >= height)
        return 0;
      else if (pos.y + y < 0)
        return 0;
    }
    return 1;
  }

  int pickParent(ArrayList<Snake> prevPop, float rnd) {
    float comp = 1;
    int index = 0;
    //it iterates backwards because the array is sorted from worst to best!
    for (int i = prevPop.size() - 1; i >= 0; i--) {
      comp -= prevPop.get(i).fitness;
      if (comp <= rnd) {
        index = i;
        break;
      }
    }
    return index;
  }

  void calcFitness() {
    fitness = score * 500 + mean(1 / sq(minDist), 1, 1 / sq(dist(pos.x, pos.y, food.pos.x, food.pos.y)), 3);
  }

  int sign(float num) {
    if (num < 0)
      return -1;
    else if (num > 0)
      return 1;
    else
      return 0;
  }

  float mean(float x, float wX, float y, float wY) {
    return (x + y) / (wX + wY);
  }
  
  int compareTo(Snake s){
    return floor(abs(fitness - s.fitness) / (fitness - s.fitness));
  }
}
