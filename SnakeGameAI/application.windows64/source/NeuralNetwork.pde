class NeuralNetwork {
  ArrayList<Neuron> inputs;
  ArrayList<ArrayList<Neuron>> hiddenLayers;
  ArrayList<Neuron> outputs;

  NeuralNetwork(int inputSize, int numHiddenLayers, int[] hiddenSize, int outputSize) {
    initializeInputLayer(inputSize);
    initializeHiddenLayer(numHiddenLayers, hiddenSize);
    initializeOutputLayer(outputSize);
  }

  //initializing algorithms
  void initializeInputLayer(int inputSize) {
    inputs = new ArrayList<Neuron>(inputSize);

    for (int i = 0; i < inputSize; i++) {
      inputs.add(i, new Neuron(0, true));
    }
  }


  void initializeHiddenLayer(int numHiddenLayers, int[] hiddenSize) {
    hiddenLayers = new ArrayList<ArrayList<Neuron>>(numHiddenLayers);

    //initializing all hidden Layersa\
    for (int i = 0; i < numHiddenLayers; i++) {
      hiddenLayers.add(i, new ArrayList<Neuron>(hiddenSize[i]));
    }

    //initializing hidden layers neurons
    for (int i = 0; i < numHiddenLayers; i++) {
      for (int j = 0; j < hiddenSize[i]; j++) {
        //i is the hidden layer and j is a neuron
        if (i == 0) // if it is the first hidden layer its inputs as the input layer values
          hiddenLayers.get(i).add(j, new Neuron(inputs.size(), false));
        else
          //A second layer of the hidden layer has the outputs of the previous layer as inputs
          hiddenLayers.get(i).add(j, new Neuron(hiddenSize[i - 1], false));
      }
    }
  }

  void initializeOutputLayer(int outputSize) {
    outputs = new ArrayList<Neuron>(outputSize);

    for (int i = 0; i < outputSize; i++) {
      //the output layer has as inputs the last hidden layer outputs
      if (hiddenLayers.size() != 0)
        outputs.add(i, new Neuron(hiddenLayers.get(hiddenLayers.size() - 1).size(), false));
      else
        outputs.add(i, new Neuron(inputs.size(), false));
    }
  }

  //initializing algorithms

  //"Comunication" algorithms
  void setInputs(float[] values) {
    for (int i = 0; i < inputs.size(); i++) {
      inputs.get(i).setInputValue(i, values);
    }
  }

  void hiddenLayersOut() {
    for (int i = 0; i < hiddenLayers.size(); i++) {
      //if it is the first hidden layer, then its inputs is the input layer values
      if (i == 0) {
        for (int j = 0; j < hiddenLayers.get(i).size(); j++) {
          hiddenLayers.get(i).get(j).setValue(inputs);
        }
      } else {
        //else, the input values are the previous hidden layer outputs
        for (int j = 0; j < hiddenLayers.get(i).size(); j++) {
          hiddenLayers.get(i).get(j).setValue(hiddenLayers.get(i - 1));
        }
      }
    }
  }

  void setOutput() {

    hiddenLayersOut();
    for (int i = 0; i < outputs.size(); i++) {
      if (hiddenLayers.size() != 0) {
        outputs.get(i).setValue(hiddenLayers.get(hiddenLayers.size() - 1));
      } else {
        outputs.get(i).setValue(inputs);
      }
    }
  }

  //getting the actual output
  float[] getOutput() {
    setOutput();
    int size = outputs.size();
    float[] outputs = new float[size];

    for (int i = 0; i < size; i++) {
      outputs[i] = this.outputs.get(i).getValue();
    }

    return outputs;
  }
}
