class SnakeComparator implements Comparator<Snake>{
  public int compare(Snake s1, Snake s2){
    return floor(abs(s1.fitness - s2.fitness) / (s1.fitness - s2.fitness));
  }
}
