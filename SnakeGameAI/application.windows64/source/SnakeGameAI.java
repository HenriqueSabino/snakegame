import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SnakeGameAI extends PApplet {



int gridSize = 30;
Snake snake;
ArrayList<Snake> population;
Food food;
boolean createNew = false;
int current = 0;
int popSize = 100;
int generation = 1;
float maxScore = 0;

public void setup() {
  
  population = new ArrayList<Snake>();
  for (int i = 0; i < popSize; i++) {
    population.add(new Snake());
  }
  snake = population.get(current);
  food = new Food(snake);
}

public void draw() {
  if (snake.score > maxScore)
    maxScore = snake.score;
  //if the snake passes too much time without eating
  //then kill it and divide its fitness by some amout
  if (frameCount - snake.lastAte >= 100 * snake.size) {
    snake.reset(true);
  }
  if (createNew) {
    current++;
    if (current == popSize) {
      repopulate();
      current = 0;
    }
    snake = population.get(current);
    snake.lastAte = frameCount;
    createNew = false;
  }
  background(51);
  fill(255);
  textSize(20);
  textAlign(LEFT, CENTER);
  text("generation - " + generation, 15, 25);
  text("population - " + popSize, 15, 50);
  text("Highscore - " + maxScore, 15, 75);
  food.show();
  snake.show();
  if (food.pos.equals(snake.pos)) {
    snake.grow();
    food.repos();
  }
  //if (frameCount % 5 == 0)
  snake.move();
}

public void repopulate() {
  float sum = 0;
  ArrayList<Snake> nextPop = new ArrayList<Snake>();
  for (int i = 0; i < popSize; i++) {
    sum += population.get(i).fitness;
  }

  for (int i = 0; i < popSize; i++) {
    population.get(i).fitness /= sum;
  }
  
  population.sort(new SnakeComparator());
  
  for (int i = 0; i < popSize; i++) {
    nextPop.add(new Snake(population));
  }

  population = nextPop;
  generation++;
}
class Food {
  PVector pos;
  Snake snake;
  Food(Snake snake) {
    pos = new PVector();
    this.snake = snake;
    repos();
  }

  public void repos() {
    pos.x = floor(random(width / gridSize)) * gridSize;
    pos.y = floor(random(height / gridSize)) * gridSize;

    for (Part p : snake.parts) {
      if (pos.x == p.x && pos.y == p.y)
        repos();
    }
  }

  public void show() {
    fill(255, 0, 0);
    ellipseMode(CORNER);
    ellipse(pos.x, pos.y, gridSize - 1, gridSize - 1);
  }
}
class NeuralNetwork {
  ArrayList<Neuron> inputs;
  ArrayList<ArrayList<Neuron>> hiddenLayers;
  ArrayList<Neuron> outputs;

  NeuralNetwork(int inputSize, int numHiddenLayers, int[] hiddenSize, int outputSize) {
    initializeInputLayer(inputSize);
    initializeHiddenLayer(numHiddenLayers, hiddenSize);
    initializeOutputLayer(outputSize);
  }

  //initializing algorithms
  public void initializeInputLayer(int inputSize) {
    inputs = new ArrayList<Neuron>(inputSize);

    for (int i = 0; i < inputSize; i++) {
      inputs.add(i, new Neuron(0, true));
    }
  }


  public void initializeHiddenLayer(int numHiddenLayers, int[] hiddenSize) {
    hiddenLayers = new ArrayList<ArrayList<Neuron>>(numHiddenLayers);

    //initializing all hidden Layersa\
    for (int i = 0; i < numHiddenLayers; i++) {
      hiddenLayers.add(i, new ArrayList<Neuron>(hiddenSize[i]));
    }

    //initializing hidden layers neurons
    for (int i = 0; i < numHiddenLayers; i++) {
      for (int j = 0; j < hiddenSize[i]; j++) {
        //i is the hidden layer and j is a neuron
        if (i == 0) // if it is the first hidden layer its inputs as the input layer values
          hiddenLayers.get(i).add(j, new Neuron(inputs.size(), false));
        else
          //A second layer of the hidden layer has the outputs of the previous layer as inputs
          hiddenLayers.get(i).add(j, new Neuron(hiddenSize[i - 1], false));
      }
    }
  }

  public void initializeOutputLayer(int outputSize) {
    outputs = new ArrayList<Neuron>(outputSize);

    for (int i = 0; i < outputSize; i++) {
      //the output layer has as inputs the last hidden layer outputs
      if (hiddenLayers.size() != 0)
        outputs.add(i, new Neuron(hiddenLayers.get(hiddenLayers.size() - 1).size(), false));
      else
        outputs.add(i, new Neuron(inputs.size(), false));
    }
  }

  //initializing algorithms

  //"Comunication" algorithms
  public void setInputs(float[] values) {
    for (int i = 0; i < inputs.size(); i++) {
      inputs.get(i).setInputValue(i, values);
    }
  }

  public void hiddenLayersOut() {
    for (int i = 0; i < hiddenLayers.size(); i++) {
      //if it is the first hidden layer, then its inputs is the input layer values
      if (i == 0) {
        for (int j = 0; j < hiddenLayers.get(i).size(); j++) {
          hiddenLayers.get(i).get(j).setValue(inputs);
        }
      } else {
        //else, the input values are the previous hidden layer outputs
        for (int j = 0; j < hiddenLayers.get(i).size(); j++) {
          hiddenLayers.get(i).get(j).setValue(hiddenLayers.get(i - 1));
        }
      }
    }
  }

  public void setOutput() {

    hiddenLayersOut();
    for (int i = 0; i < outputs.size(); i++) {
      if (hiddenLayers.size() != 0) {
        outputs.get(i).setValue(hiddenLayers.get(hiddenLayers.size() - 1));
      } else {
        outputs.get(i).setValue(inputs);
      }
    }
  }

  //getting the actual output
  public float[] getOutput() {
    setOutput();
    int size = outputs.size();
    float[] outputs = new float[size];

    for (int i = 0; i < size; i++) {
      outputs[i] = this.outputs.get(i).getValue();
    }

    return outputs;
  }
}
class Neuron {
  int inNum, outNum;
  float bias;
  float[] inWeights;
  boolean input;
  float value;


  Neuron(int inNum, boolean input) {
    this.inNum = inNum;
    this.input = input;

    inWeights = new float[inNum];
    if (!input)
      bias = random(-1, 1);
    else
      bias = 0;

    for (int i = 0; i < inNum; i++) {
      inWeights[i] = random(-1, 1);
    }
  }

  public void setInputValue(int index, float[] values) {
    //A input neuron only has 1 input, and that is its output, too
    value = values[index];
  }

  public void setValue(ArrayList<Neuron> neurons) {
    //using the activating function
    for (int i = 0; i < inNum; i++) {
      value += inWeights[i] * neurons.get(i).value;
    }
    
    value += bias;
    value = sig(value);
  }

  public float getValue() {
    return value;
  }

  public float sig(float x) {
    return (logisSig(x) - 0.5f) * 2;
  }

  public float logisSig(float x) {
    return 1 / (1 + exp(-2 * x));
  }

  public float relu(float x) {
    return max(0, x);
  }

  public float linear(float x) {
    return x;
  }

  public float expo(float x) {
    return exp(x);
  }

  public float softPlus(float x) {
    return log(1 + exp(x));
  }

  public float simHardLimit (float x) {
    if (x > 0)
      return 1;
    else if (x == 0)
      return 0;
    else
      return -1;
  }
}
class Part {
  int x, y;
  Part(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
class Snake implements Comparable<Snake>{
  private int size = 4;
  private PVector dir;
  public ArrayList<Part> parts;
  public PVector pos;
  public int score = 0;
  public NeuralNetwork neuNet;
  public float fitness = 0;
  float minDist = 100000;
  //frame that it eats the food
  //avoids the snake to spins in circles
  float lastAte;

  Snake() {
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
    neuNet = new NeuralNetwork(6, 1, new int[] {10}, 4);
  }

  //genetic algorithm
  Snake(ArrayList<Snake> prevPop) {
    dir = new PVector(1, 0);
    parts = new ArrayList<Part>();
    for (int i = 0; i < size; i++) {
      int xpos = floor(width / 2);
      parts.add(new Part(xpos - i * gridSize, floor(height / 2)));
    }
    pos = new PVector(parts.get(0).x, parts.get(0).y);
    neuNet = new NeuralNetwork(6, 1, new int[] {10}, 4);

    Snake parent1 = prevPop.get(pickParent(prevPop, random(1)));
    Snake parent2 = prevPop.get(pickParent(prevPop, random(1)));

    //note that it does not need to inherit the weights of the input layer because there are no weights in the layer
    //inheriting the hidden layers weights and biases
    for (int i = 0; i < parent1.neuNet.hiddenLayers.size(); i++) {
      for (int j = 0; j < parent1.neuNet.hiddenLayers.get(i).size(); j++) {
        for (int k = 0; k < parent1.neuNet.hiddenLayers.get(i).get(j).inWeights.length; k++) {
          //mutation rate
          if (random(1) >= 0.03f) {
            if (random(1) >= 0.5f) {
              this.neuNet.hiddenLayers.get(i).get(j).inWeights[k] = parent1.neuNet.hiddenLayers.get(i).get(j).inWeights[k];
              this.neuNet.hiddenLayers.get(i).get(j).bias = parent1.neuNet.hiddenLayers.get(i).get(j).bias;
            } else {
              this.neuNet.hiddenLayers.get(i).get(j).inWeights[k] = parent2.neuNet.hiddenLayers.get(i).get(j).inWeights[k];
              this.neuNet.hiddenLayers.get(i).get(j).bias = parent2.neuNet.hiddenLayers.get(i).get(j).bias;
            }
          }
          //if it will mutate
          else {
            this.neuNet.hiddenLayers.get(i).get(j).inWeights[k] = random(-1, 1);
            this.neuNet.hiddenLayers.get(i).get(j).bias = random(-1, 1);
          }
        }
      }
    }

    //inheriting the outputs weights and biases
    for (int i = 0; i < parent1.neuNet.outputs.size(); i++) {
      for (int j = 0; j < parent1.neuNet.outputs.get(0).inWeights.length; j++) {
        //mutation rate
        if (random(1) >= 0.03f) {
          if (random(1) >= 0.5f) {
            this.neuNet.outputs.get(i).inWeights[j] = parent1.neuNet.outputs.get(i).inWeights[j];
            this.neuNet.outputs.get(i).bias = parent1.neuNet.outputs.get(i).bias;
          } else {
            this.neuNet.outputs.get(i).inWeights[j] = parent2.neuNet.outputs.get(i).inWeights[j];
            this.neuNet.outputs.get(i).bias = parent2.neuNet.outputs.get(i).bias;
          }
        }
        //if it will mutate
        else {
          this.neuNet.outputs.get(i).inWeights[j] = random(-1, 1);
          this.neuNet.outputs.get(i).bias = random(-1, 1);
        }
      }
    }
  }
  //genetic algorithm

  public void show() {
    fill(255);
    noStroke();
    for (Part p : parts) {
      rect(p.x, p.y, gridSize - 1, gridSize - 1);
    }
  }

  public void move() {  
    float foodDirX = food.pos.x - pos.x;
    float foodDirY = food.pos.y - pos.y;
    //distance to wall and distance to the food
    float[] inputs = new float[] {foodDirX, foodDirY, checkNextStep(1, 0), checkNextStep(-1, 0), checkNextStep(0, 1), checkNextStep(0, -1)};
    //sets the input to the Neural Network
    neuNet.setInputs(inputs);
    //gets the output of the Neural Network
    float[] nextDir = neuNet.getOutput();
    updateDir(nextDir[0], nextDir[1], nextDir[2], nextDir[3]);

    //updates all body parts positions
    for (int i = size - 1; i >= 1; i--) {
      parts.get(i).x = parts.get(i - 1).x;
      parts.get(i).y = parts.get(i - 1).y;
    }
    //updates the head position
    parts.get(0).x += dir.x * gridSize;
    parts.get(0).y += dir.y * gridSize;

    checkEdges();
    float currentDist = dist(pos.x, pos.y, food.pos.x, food.pos.y);
    if (currentDist < minDist)
      minDist = currentDist;

    pos = new PVector(parts.get(0).x, parts.get(0).y);

    checkDeath();
  }

  public void updateDir(float u, float d, float l, float r) {
    FloatList temp = new FloatList();
    temp.append(u);
    temp.append(d); 
    temp.append(l); 
    temp.append(r);
    temp.sortReverse();
    if (temp.get(0) == u) {
      int x = 0, y = -1;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    } else if (temp.get(0) == d) {
      int x = 0, y = 1;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    } else if (temp.get(0) == l) {
      int x = -1, y = 0;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    } else if (temp.get(0) == r) {
      int x = 1, y = 0;
      dir.x = (pos.x + x * gridSize == parts.get(1).x)? dir.x : x;
      dir.y = (pos.y + y * gridSize == parts.get(1).y)? dir.y : y;
    }
  }

  public void checkEdges() {
    if (parts.get(0).x >= width)
      //parts.get(0).x = 0;
      reset(false);
    else if (parts.get(0).x < 0)
      //parts.get(0).x = width;
      reset(false);
    if (parts.get(0).y >= height)
      //parts.get(0).y = 0;
      reset(false);
    else if (parts.get(0).y < 0)
      //parts.get(0).y = height;
      reset(false);
  }

  public void grow() {
    size++;
    score += 10;
    lastAte = frameCount;
    parts.add(new Part((int)pos.x, (int)pos.y));
  }

  public void reset(boolean spinning) {
    calcFitness();
    if (spinning)
      this.fitness /= 5000;
    createNew = true;
  }

  public void checkDeath() {
    for (int i = 1; i < size; i++) {
      if (pos.x == parts.get(i).x && pos.y == parts.get(i).y) {
        reset(false);
      }
    }
  }

  public int checkNextStep(int x, int y) {
    for (int i = 1; i < size; i++) {
      if (pos.x + x == parts.get(i).x && pos.y + y == parts.get(i).y) {
        return 0;
      }
      if (pos.x + x >= width)
        return 0;
      else if (pos.x + x < 0)
        return 0;
      if (pos.y + y >= height)
        return 0;
      else if (pos.y + y < 0)
        return 0;
    }
    return 1;
  }

  public int pickParent(ArrayList<Snake> prevPop, float rnd) {
    float comp = 1;
    int index = 0;
    //it iterates backwards because the array is sorted from worst to best!
    for (int i = prevPop.size() - 1; i >= 0; i--) {
      comp -= prevPop.get(i).fitness;
      if (comp <= rnd) {
        index = i;
        break;
      }
    }
    return index;
  }

  public void calcFitness() {
    fitness = score * 500 + mean(1 / sq(minDist), 1, 1 / sq(dist(pos.x, pos.y, food.pos.x, food.pos.y)), 3);
  }

  public int sign(float num) {
    if (num < 0)
      return -1;
    else if (num > 0)
      return 1;
    else
      return 0;
  }

  public float mean(float x, float wX, float y, float wY) {
    return (x + y) / (wX + wY);
  }
  
  public int compareTo(Snake s){
    return floor(abs(fitness - s.fitness) / (fitness - s.fitness));
  }
}
class SnakeComparator implements Comparator<Snake>{
  public int compare(Snake s1, Snake s2){
    return floor(abs(s1.fitness - s2.fitness) / (s1.fitness - s2.fitness));
  }
}
  public void settings() {  size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "SnakeGameAI" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
