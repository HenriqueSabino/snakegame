class Neuron {
  int inNum, outNum;
  float bias;
  float[] inWeights;
  boolean input;
  float value;


  Neuron(int inNum, boolean input) {
    this.inNum = inNum;
    this.input = input;

    inWeights = new float[inNum];
    if (!input)
      bias = random(-1, 1);
    else
      bias = 0;

    for (int i = 0; i < inNum; i++) {
      inWeights[i] = random(-1, 1);
    }
  }

  void setInputValue(int index, float[] values) {
    //A input neuron only has 1 input, and that is its output, too
    value = values[index];
  }

  void setValue(ArrayList<Neuron> neurons) {
    //using the activating function
    for (int i = 0; i < inNum; i++) {
      value += inWeights[i] * neurons.get(i).value;
    }
    
    value += bias;
    value = sig(value);
  }

  float getValue() {
    return value;
  }

  float sig(float x) {
    return (logisSig(x) - 0.5) * 2;
  }

  float logisSig(float x) {
    return 1 / (1 + exp(-2 * x));
  }

  float relu(float x) {
    return max(0, x);
  }

  float linear(float x) {
    return x;
  }

  float expo(float x) {
    return exp(x);
  }

  float softPlus(float x) {
    return log(1 + exp(x));
  }

  float simHardLimit (float x) {
    if (x > 0)
      return 1;
    else if (x == 0)
      return 0;
    else
      return -1;
  }
}
